module.exports = (request, reply) => {
  return reply.send({ oops: 'this is not the path you\'re looking for' })
}
