const mongoose = require('mongoose')

const user = process.env.DATABASE_USERNAME
const pass = process.env.DATABASE_PASSWORD
const host = process.env.DATABASE_HOST
const port = process.env.DATABASE_PORT
const data = process.env.DATABASE_DATABASE

const connection = (process.env.NODE_ENV === 'local')
  ? `mongodb://${host}:27017/test`
  : `mongodb://${user}:${pass}@${host}:${port}/${data}`

mongoose.connect(connection, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})

const shutdown = async () => {
  await mongoose.connection.close()
  process.exit(0)
}

process.on('SIGINT', shutdown)
process.on('SIGUSR1', shutdown)
process.on('SIGUSR2', shutdown)

module.exports = { mongoose }
