const { version } = require('../../../package.json')

module.exports = (fastify, opts, done) => {
  fastify.get('/', async (request, reply) => {
    return reply.send({ service: 'failsafe', version })
  })

  done()
}
