resource "kubernetes_deployment" "failsafe" {
  depends_on = [helm_release.failsafe]

  metadata {
    name = "failsafe"
    labels = {
      app = "failsafe"
    }
  }

  spec {
    replicas = 2

    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "50%"
      }
    }

    selector {
      match_labels = {
        app = "failsafe"
      }
    }

    template {
      metadata {
        labels = {
          app = "failsafe"
        }
      }

      spec {
        container {
          name              = "failsafe"
          image             = "registry.digitalocean.com/o9k/failsafe:0.0.1"
          image_pull_policy = "Always"

          port {
            container_port = "4000"
            host_port      = "4000"
          }

          liveness_probe {
            http_get {
              path = "/"
              port = 4000
            }
          }

          env {
            name  = "DATABASE_USERNAME"
            value = random_string.database_username.result
          }

          env {
            name  = "DATABASE_PASSWORD"
            value = random_password.database_password.result
          }

          env {
            name  = "DATABASE_DATABASE"
            value = random_string.database_database.result
          }

          env {
            name  = "DATABASE_HOST"
            value = "failsafe-mongodb"
          }

          env {
            name  = "DATABASE_PORT"
            value = "27017"
          }

          volume_mount {
            name       = "keys"
            mount_path = "/usr/src/failsafe/keys"
            read_only  = true
          }
        }

        volume {
          name = "keys"
          secret {
            secret_name = "keys"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "failsafe" {
  depends_on = [kubernetes_deployment.failsafe]

  metadata {
    name = "failsafe"
  }

  spec {
    session_affinity = "ClientIP"
    selector = {
      app = "failsafe"
    }

    port {
      port        = 4000
      target_port = 4000
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress" "failsafe" {
  depends_on = [kubernetes_service.failsafe]

  metadata {
    name = "failsafe"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }

  spec {
    rule {
      host = "failsafe.o9k.io"
      http {
        path {
          path = "/"
          backend {
            service_name = "failsafe"
            service_port = "4000"
          }
        }
      }
    }

    tls {
      hosts       = ["o9k.io", "*.o9k.io"]
      secret_name = "o9kio"
    }
  }
}


