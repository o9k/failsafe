FROM node:14-alpine

VOLUME ["/usr/src/failsafe/keys"]

WORKDIR /usr/src/failsafe

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 4000

CMD ["npm", "start"]

